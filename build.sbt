ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.0"

lazy val root = (project in file("."))
  .settings(
    name := "puzzler-2022",
    scalacOptions ++= Seq("-Yrecursion", "1000"),
    Compile / mainClass := Some("com.matthew.puzzler.Main"),
    run / mainClass := Some("com.matthew.puzzler.Main")
  )

libraryDependencies += "org.scalactic" %% "scalactic" % "3.2.14"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.14" % "test"
