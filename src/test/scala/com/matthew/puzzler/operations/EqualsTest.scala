package com.matthew.puzzler.operations

import com.matthew.puzzler.solver.a
import org.scalatest.flatspec.AnyFlatSpec

class EqualsTest extends AnyFlatSpec {
  "A letter" should "equal itself" in {
    Equals[a, a]()
  }

  "Two different letters" should "not equal each other" in {
    // Equals[a, b]()
    // Type argument com.matthew.puzzler.a does not conform to upper bound com.matthew.puzzler.b
  }
}
