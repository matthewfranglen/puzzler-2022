package com.matthew.puzzler.values

import com.matthew.puzzler.operations.Comparison.show
import com.matthew.puzzler.values.Bool.toBoolean
import com.matthew.puzzler.values.Num._
import org.scalatest.flatspec.AnyFlatSpec

class NumTest extends AnyFlatSpec {
  "Zero" should "match IfZero type" in {
    assert(toBoolean[_0#Match[Constant[False]#Value, True, Bool]])
  }

  "Zero" should "be equal to zero" in {
    assert(show[_0#Compare[_0]] == "eq")
  }

  "Zero" should "not compare as less than zero" in {
    assert(!toBoolean[_0#Compare[_0]#lt])
  }

  "Zero" should "compare as less than or equal to zero" in {
    assert(toBoolean[_0#Compare[_0]#le])
  }

  "Zero" should "compare as equal to zero" in {
    assert(toBoolean[_0#Compare[_0]#eq])
  }

  "Zero" should "compare as greater than or equal to zero" in {
    assert(toBoolean[_0#Compare[_0]#ge])
  }

  "Zero" should "not compare as greater than to zero" in {
    assert(!toBoolean[_0#Compare[_0]#gt])
  }

  "Zero" should "be less than one, two or three" in {
    assert(show[_0#Compare[_1]] == "lt")
    assert(show[_0#Compare[_2]] == "lt")
    assert(show[_0#Compare[_3]] == "lt")
  }

  "Zero" should "compare as less than one, two or three" in {
    assert(toBoolean[_0#Compare[_1]#lt])
    assert(toBoolean[_0#Compare[_2]#lt])
    assert(toBoolean[_0#Compare[_3]#lt])
  }

  "Zero" should "compare as less than or equal to one, two or three" in {
    assert(toBoolean[_0#Compare[_1]#le])
    assert(toBoolean[_0#Compare[_2]#le])
    assert(toBoolean[_0#Compare[_3]#le])
  }

  "Zero" should "not compare as equal to one, two or three" in {
    assert(!toBoolean[_0#Compare[_1]#eq])
    assert(!toBoolean[_0#Compare[_2]#eq])
    assert(!toBoolean[_0#Compare[_3]#eq])
  }

  "Zero" should "not compare as greater than or equal to one, two or three" in {
    assert(!toBoolean[_0#Compare[_1]#ge])
    assert(!toBoolean[_0#Compare[_2]#ge])
    assert(!toBoolean[_0#Compare[_3]#ge])
  }

  "Zero" should "not compare as greater than to one, two or three" in {
    assert(!toBoolean[_0#Compare[_1]#gt])
    assert(!toBoolean[_0#Compare[_2]#gt])
    assert(!toBoolean[_0#Compare[_3]#gt])
  }

  "Zero" should "be represented by the number 0" in {
    assert(toInt[_0] == 0)
  }

  "One, Two and Three" should "match NonZero type" in {
    assert(toBoolean[_1#Match[Constant[True]#Value, False, Bool]])
    assert(toBoolean[_2#Match[Constant[True]#Value, False, Bool]])
    assert(toBoolean[_3#Match[Constant[True]#Value, False, Bool]])
  }

  "One, Two and Three" should "be greater than zero" in {
    assert(show[_1#Compare[_0]] == "gt")
    assert(show[_2#Compare[_0]] == "gt")
    assert(show[_3#Compare[_0]] == "gt")
  }

  "One, Two and Three" should "not compare as less than zero" in {
    assert(!toBoolean[_1#Compare[_0]#lt])
    assert(!toBoolean[_2#Compare[_0]#lt])
    assert(!toBoolean[_3#Compare[_0]#lt])
  }

  "One, Two and Three" should "not compare as less than or equal to zero" in {
    assert(!toBoolean[_1#Compare[_0]#le])
    assert(!toBoolean[_2#Compare[_0]#le])
    assert(!toBoolean[_3#Compare[_0]#le])
  }

  "One, Two and Three" should "not compare as equal to zero" in {
    assert(!toBoolean[_1#Compare[_0]#eq])
    assert(!toBoolean[_2#Compare[_0]#eq])
    assert(!toBoolean[_3#Compare[_0]#eq])
  }

  "One, Two and Three" should "compare as greater than or equal to zero" in {
    assert(toBoolean[_1#Compare[_0]#ge])
    assert(toBoolean[_2#Compare[_0]#ge])
    assert(toBoolean[_3#Compare[_0]#ge])
  }

  "One, Two and Three" should "compare as greater than zero" in {
    assert(toBoolean[_1#Compare[_0]#gt])
    assert(toBoolean[_2#Compare[_0]#gt])
    assert(toBoolean[_3#Compare[_0]#gt])
  }

  "One, Two and Three" should "be represented by the numbers 1, 2 and 3" in {
    assert(toInt[_1] == 1)
    assert(toInt[_2] == 2)
    assert(toInt[_3] == 3)
  }

  "Zero add One" should "be One" in {
    assert(toInt[_0 + _1] == 1)
  }

  "One add One" should "be Two" in {
    assert(toInt[_1 + _1] == 2)
  }

  "NonZero" should "permit stepping down to zero" in {
    type TraverseOnce[Number <: Num] = Number#Match[Constant[False]#Value, True, Bool]
    assert(toBoolean[
      _1#Match[
        TraverseOnce,
        False,
        Bool,
      ]
    ])
  }

  "Zero" should "represent as 0" in {
    assert(toInt[_0] == 0)
  }

  "Zero#inc" should "represent as 0" in {
    assert(toInt[_0#inc] == 1)
  }

  "Zero#dec" should "represent as 0" in {
    assert(toInt[_0#dec] == 0)
  }

  "One" should "represent as 1" in {
    assert(toInt[_1] == 1)
  }

  "One#inc" should "represent as 2" in {
    assert(toInt[_1#inc] == 2)
  }

  "One#dec" should "represent as 0" in {
    assert(toInt[_1#dec] == 0)
  }
}
