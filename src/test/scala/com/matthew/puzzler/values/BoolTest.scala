package com.matthew.puzzler.values

import com.matthew.puzzler.values.Bool._
import org.scalatest.flatspec.AnyFlatSpec

class BoolTest extends AnyFlatSpec {
  "True" should "be True" in {
    assert(toBoolean[True])
  }

  "False" should "be False" in {
    assert(!toBoolean[False])
  }

  "And" should "be true only when both operands are true" in {
    assert(toBoolean[True && True])
    assert(!toBoolean[True && False])
    assert(!toBoolean[False && True])
    assert(!toBoolean[False && False])
  }

  "Or" should "be true when either or both operands are true" in {
    assert(toBoolean[True || True])
    assert(toBoolean[True || False])
    assert(toBoolean[False || True])
    assert(!toBoolean[False || False])
  }

  "Not" should "be true when the operand is false" in {
    assert(toBoolean[Not[False]])
    assert(!toBoolean[Not[True]])
  }
}
