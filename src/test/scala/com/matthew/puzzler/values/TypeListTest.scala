package com.matthew.puzzler.values

import com.matthew.puzzler.operations.{Equals, UnaryFunction}
import com.matthew.puzzler.values.Bool.toBoolean
import com.matthew.puzzler.values.Num._
import org.scalatest.flatspec.AnyFlatSpec

class TypeListTest extends AnyFlatSpec {
  "TypeListEnd" should "return the empty value when applied" in {
    type list = TypeListEnd[Any]
    type value = list#Apply[
      UnaryFunction.ToConstant[_, True], // this is returned when the list is not empty
      False, // this is returned if the list is empty
      Bool
    ]
    assert(!toBoolean[value])
  }

  "TypeListEnd" should "return the empty value when applied to the head value" in {
    type list = TypeListEnd[Any]
    type value = list#ApplyHead[
      UnaryFunction.ToConstant[_, True], // this is returned when the list is not empty
      False, // this is returned if the list is empty
      Bool
    ]
    assert(!toBoolean[value])
  }

  "TypeListEnd" should "return type list end when filtered" in {
    type list = TypeListEnd[Any]
    type filtered = list#Filter[UnaryFunction.ToConstant[_, True]]
    assert(toInt[filtered#length] == 0)
  }

  "TypeListEnd" should "return type list end when filtered by the head value" in {
    type list = TypeListEnd[Any]
    type filtered = list#FilterHead[UnaryFunction.ToConstant[_, True]]
    assert(toInt[filtered#length] == 0)
  }

  "TypeListEnd" should "return Nothing for Head" in {
    type list = TypeListEnd[Any]
    Equals[list#head, Nothing]()
  }

  "TypeListEnd" should "return Nothing for Tail" in {
    type list = TypeListEnd[Any]
    Equals[list#tail, Nothing]()
  }

  "TypeListEnd" should "return an empty list for select index" in {
    type list = TypeListEnd[Any]
    assert(toBoolean[list#SelectIndex[_3]#IsEmpty])
    assert(toBoolean[list#SelectIndex[_2]#IsEmpty])
    assert(toBoolean[list#SelectIndex[_1]#IsEmpty])
    assert(toBoolean[list#SelectIndex[_0]#IsEmpty])
  }

  "TypeListEnd" should "return the other list when concatenated" in {
    type empty = TypeListEnd[Num]
    type other = _3#range

    implicitly[empty#Concatenate[other] =:= other]
    implicitly[other#Concatenate[empty] =:= other]
  }

  "TypeListEntry" should "return an empty list when zipped" in {
    assert(toBoolean[TypeListEnd[Any]#Zip[_3#range, Num]#IsEmpty])
    assert(toBoolean[_3#range#Zip[TypeListEnd[Any], Any]#IsEmpty])
  }

  "TypeListEnd" should "have a length of zero" in {
    type list = TypeListEnd[Any]
    assert(toInt[list#length] == 0)
  }

  "TypeListEntry" should "return the mapped value when applied" in {
    type list = TypeListEntry[_0, TypeListEnd[Num], Num]
    type value = list#Apply[
      UnaryFunction.ToConstant[_, True], // this is returned when the list is not empty
      False, // this is returned if the list is empty
      Bool
    ]
    assert(toBoolean[value])
  }

  "TypeListEntry" should "return the mapped value when applied to the head value" in {
    type list = TypeListEntry[_0, TypeListEnd[Num], Num]
    type value = list#ApplyHead[
      UnaryFunction.ToConstant[_, True], // this is returned when the list is not empty
      False, // this is returned if the list is empty
      Bool
    ]
    assert(toBoolean[value])
  }

  "TypeListEntry" should "return a transformed list when mapped" in {
    type list = _1#range
    type mapped = list#MapHead[UnaryFunction.Inc, Num]
    assert(toInt[mapped#head] == 2)
  }

  "TypeListEntry" should "return an empty list when every value is filtered" in {
    type list = _1#range
    trait GreaterThanTwo extends UnaryFunction[Num, Bool] {
      type Apply[N <: Num] = N#Compare[_2]#gt
    }
    type filtered = list#FilterHead[GreaterThanTwo]
    assert(toBoolean[filtered#IsEmpty])
  }

  "TypeListEntry" should "return a non empty list when some values pass the filter" in {
    type list = _3#range
    trait GreaterThanTwo extends UnaryFunction[Num, Bool] {
      type Apply[N <: Num] = N#Compare[_2]#gt
    }
    type filtered = list#FilterHead[GreaterThanTwo]
    assert(!toBoolean[filtered#IsEmpty])
  }

  "TypeListEntry" should "return a list with the correct values when filtered" in {
    type list = _3#range
    trait GreaterThanTwo extends UnaryFunction[Num, Bool] {
      type Apply[N <: Num] = N#Compare[_2]#gt
    }
    type filtered = list#FilterHead[GreaterThanTwo]
    assert(toBoolean[filtered#head#Compare[_3]#eq])
    assert(toBoolean[filtered#tail#IsEmpty])
  }

  "TypeListEntry" should "return true for match all if all values pass the predicate" in {
    type list = _2#range
    trait LessThanThree extends UnaryFunction[Num, Bool] {
      type Apply[N <: Num] = N#Compare[_3]#lt
    }
    assert(toBoolean[list#MatchAllHead[LessThanThree]])
  }

  "TypeListEntry" should "return false for match all if any values fail the predicate" in {
    type list = _3#range
    trait LessThanThree extends UnaryFunction[Num, Bool] {
      type Apply[N <: Num] = N#Compare[_3]#lt
    }
    assert(!toBoolean[list#MatchAllHead[LessThanThree]])
  }

  "TypeListEntry" should "return false for match all if all values fail the predicate" in {
    type list = _3#range
    trait GreaterThanThree extends UnaryFunction[Num, Bool] {
      type Apply[N <: Num] = N#Compare[_3]#gt
    }
    assert(!toBoolean[list#MatchAllHead[GreaterThanThree]])
  }

  "TypeListEntry" should "return true for match any if all values pass the predicate" in {
    type list = _2#range
    trait LessThanThree extends UnaryFunction[Num, Bool] {
      type Apply[N <: Num] = N#Compare[_3]#lt
    }
    assert(toBoolean[list#MatchAnyHead[LessThanThree]])
  }

  "TypeListEntry" should "return true for match any if any values fail the predicate" in {
    type list = _3#range
    trait LessThanThree extends UnaryFunction[Num, Bool] {
      type Apply[N <: Num] = N#Compare[_3]#lt
    }
    assert(toBoolean[list#MatchAnyHead[LessThanThree]])
  }

  "TypeListEntry" should "return false for match any if all values fail the predicate" in {
    type list = _3#range
    trait GreaterThanThree extends UnaryFunction[Num, Bool] {
      type Apply[N <: Num] = N#Compare[_3]#gt
    }
    assert(!toBoolean[list#MatchAnyHead[GreaterThanThree]])
  }

  "TypeListEntry" should "return a list of the appropriate length for select index" in {
    type list = _4#range
    assert(toInt[list#SelectIndex[_3]#length] == 4)
    assert(toInt[list#SelectIndex[_2]#length] == 3)
    assert(toInt[list#SelectIndex[_1]#length] == 2)
    assert(toInt[list#SelectIndex[_0]#length] == 1)
  }

  "TypeListEntry" should "return a list of pairs when zipped" in {
    type list = _4#range
    type zipped = list#Zip[_3#range, Num]
    assert(toInt[zipped#head#left] == 4)
    assert(toInt[zipped#head#right] == 3)
  }

  "TypeListEntry" should "return a list that is as long as the shortest list when zipped" in {
    assert(toInt[_4#range#Zip[_3#range, Num]#length] == 3)
    assert(toInt[_3#range#Zip[_4#range, Num]#length] == 3)
  }

  "TypeListEntry" should "return the combined list when concatenated with another" in {
    type list = _2#range
    type other = _3#range

    assert(toInt[list#Concatenate[other]#length] == 5)
    assert(toInt[other#Concatenate[list]#length] == 5)
  }

  "TypeListEntry[_0]" should "return _0 for Head" in {
    type list = TypeListEntry[_0, TypeListEnd[Num], Num]
    implicitly[list#head =:= _0]
  }

  "TypeListEntry[_, TypeListEnd]" should "return TypeListEnd for Tail" in {
    type list = TypeListEntry[_0, TypeListEnd[Num], Num]
    implicitly[list#tail =:= TypeListEnd[Num]]
  }

  "TypeListEntry[_, TypeListEnd]" should "have a length of one" in {
    type list = TypeListEntry[_0, TypeListEnd[Num], Num]
    assert(toInt[list#length] == 1)
  }

  "_0#range" should "produce an empty range" in {
    type list = _0#range
    assert(toInt[list#length] == 0)
  }

  "_1#range" should "produce an range of length 1" in {
    type list = _1#range
    assert(toInt[list#length] == 1)
  }

  "_3#range" should "produce an range of length 3" in {
    type list = _3#range
    assert(toInt[list#length] == 3)
  }
}
