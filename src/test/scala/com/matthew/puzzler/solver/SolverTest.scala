package com.matthew.puzzler.solver

import com.matthew.puzzler.values.Bool.toBoolean
import org.scalatest.flatspec.AnyFlatSpec

class SolverTest extends AnyFlatSpec {

  "A word in the word list" should "get solved" in {
    type solution = Solver.Solve[ValidWords.Words, Word.make[a, b, o, d, e]]
    assert(toBoolean[solution#IsSolved])
  }

  "A word not in the word list" should "not get solved" in {
    type solution = Solver.Solve[ValidWords.Words, Word.make[a, b, b, b, a]]
    assert(!toBoolean[solution#IsSolved])
  }

}
