package com.matthew.puzzler.solver

import com.matthew.puzzler.values.Bool.toBoolean
import com.matthew.puzzler.values.Num._
import com.matthew.puzzler.values.TypeListEnd
import org.scalatest.flatspec.AnyFlatSpec
import com.matthew.puzzler.solver.Word._

class WordTest extends AnyFlatSpec {
  "EmptyWord" should "match empty word" in {
    assert(toBoolean[Word.MatchesWord[TypeListEnd[Letter], TypeListEnd[Letter]]])
  }

  "EmptyWord" should "not match non empty word" in {
    type word = a :: TypeListEnd[Letter]
    assert(!toBoolean[Word.MatchesWord[word, TypeListEnd[Letter]]])
    assert(!toBoolean[Word.MatchesWord[TypeListEnd[Letter], word]])
  }

  "LetterWord" should "match same word" in {
    type word = a :: TypeListEnd[Letter]
    assert(toBoolean[Word.MatchesWord[word, word]])
  }

  "LetterWord" should "not match different length word" in {
    type word = a :: TypeListEnd[Letter]
    type longerWord = a ::: a
    assert(!toBoolean[Word.MatchesWord[word, longerWord]])
    assert(!toBoolean[Word.MatchesWord[longerWord, word]])
  }

  "dog" should "contain the letters d, o and g" in {
    type word = d :: o ::: g
    assert(toBoolean[Word.ContainsLetter[word, d]])
    assert(toBoolean[Word.ContainsLetter[word, o]])
    assert(toBoolean[Word.ContainsLetter[word, g]])
  }

  "dog" should "not contain the letters c, a or t" in {
    type word = d :: o ::: g
    assert(!toBoolean[Word.ContainsLetter[word, c]])
    assert(!toBoolean[Word.ContainsLetter[word, a]])
    assert(!toBoolean[Word.ContainsLetter[word, t]])
  }

  "dog" should "match the letters d, o and g" in {
    type word = d :: o ::: g
    assert(toBoolean[Word.MatchesLetter[word, d, _2]])
    assert(toBoolean[Word.MatchesLetter[word, o, _1]])
    assert(toBoolean[Word.MatchesLetter[word, g, _0]])
  }

  // "dog" should "convert to the string 'dog'" in {
  //   type word = d :: o ::: g
  //   assert(toString[word] == "dog")
  // }
}
