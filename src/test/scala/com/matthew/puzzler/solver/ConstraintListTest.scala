package com.matthew.puzzler.solver

import com.matthew.puzzler.solver.Word._
import com.matthew.puzzler.values.Bool.toBoolean
import com.matthew.puzzler.values.Num._
import com.matthew.puzzler.values.TypeListEnd
import org.scalatest.flatspec.AnyFlatSpec

class ConstraintListTest extends AnyFlatSpec {
  "Grey[a], Grey[b]" should "not exclude dog" in {
    type constraints = TypeListEnd[Constraint]#Add[Grey[a]#ToConstraint[_0]]#Add[Grey[b]#ToConstraint[_1]]
    assert(toBoolean[ConstraintList.FilterWord[constraints, d :: o ::: g]])
  }

  "Grey[a], Grey[b]" should "exclude cat" in {
    type constraints = TypeListEnd[Constraint]#Add[Grey[a]#ToConstraint[_0]]#Add[Grey[b]#ToConstraint[_1]]
    assert(!toBoolean[ConstraintList.FilterWord[constraints, c :: a ::: t]])
  }

  "Yellow[a, 0], Yellow[c, 1]" should "not exclude cat" in {
    type constraints = TypeListEnd[Constraint]#Add[Yellow[a]#ToConstraint[_0]]#Add[Yellow[c]#ToConstraint[_1]]
    assert(toBoolean[ConstraintList.FilterWord[constraints, c :: a ::: t]])
  }

  "Yellow[a, 0], Yellow[o, 1]" should "exclude dog" in {
    type constraints = TypeListEnd[Constraint]#Add[Yellow[a]#ToConstraint[_0]]#Add[Yellow[o]#ToConstraint[_1]]
    assert(!toBoolean[ConstraintList.FilterWord[constraints, d :: o ::: g]])
  }

  "Green[t, 0], Green[c, 2]" should "not exclude cat" in {
    type constraints = TypeListEnd[Constraint]#Add[Green[t]#ToConstraint[_0]]#Add[Green[c]#ToConstraint[_2]]
    assert(toBoolean[ConstraintList.FilterWord[constraints, c :: a ::: t]])
  }

  "Green[t, 0], Green[o, 1]" should "exclude dog" in {
    type constraints = TypeListEnd[Constraint]#Add[Green[t]#ToConstraint[_0]]#Add[Green[o]#ToConstraint[_1]]
    assert(!toBoolean[ConstraintList.FilterWord[constraints, d :: o ::: g]])
  }
}