package com.matthew.puzzler.solver

import com.matthew.puzzler.solver.Word._
import com.matthew.puzzler.values.Num._
import com.matthew.puzzler.values.{TypeList, TypeListEnd}
import org.scalatest.flatspec.AnyFlatSpec

class WordListTest extends AnyFlatSpec {
  type words = TypeListEnd[TypeList[Letter]]#Add[c :: a ::: t]#Add[d :: o ::: g]

  "Grey[z]" should "exclude no words" in {
    type constraint = Grey[z]#ToConstraint[_0]
    assert(toInt[WordList.FilterConstraint[words, constraint]#length] == 2)
  }

  "Grey[d]" should "exclude one word" in {
    type constraint = Grey[d]#ToConstraint[_0]
    assert(toInt[WordList.FilterConstraint[words, constraint]#length] == 1)
  }

  "Yellow[d, 0]" should "exclude one word" in {
    // cat does not include d
    type constraint = Yellow[d]#ToConstraint[_0]
    assert(toInt[WordList.FilterConstraint[words, constraint]#length] == 1)
  }

  "Yellow[d, 2]" should "exclude two words" in {
    // cat does not include d, and dog has d in the excluded position
    type constraint = Yellow[d]#ToConstraint[_2]
    assert(toInt[WordList.FilterConstraint[words, constraint]#length] == 0)
  }

  "Green[d, 2]" should "exclude one word" in {
    // cat does not include d
    type constraint = Green[d]#ToConstraint[_2]
    assert(toInt[WordList.FilterConstraint[words, constraint]#length] == 1)
  }

  "Green[d, 0]" should "exclude two words" in {
    // cat does not include d, and dog has d in the wrong position
    type constraint = Green[d]#ToConstraint[_0]
    assert(toInt[WordList.FilterConstraint[words, constraint]#length] == 0)
  }

  "constraint list Grey[z]" should "exclude no words" in {
    type constraints = TypeListEnd[Constraint]#Add[Grey[z]#ToConstraint[_0]]#Add[Grey[y]#ToConstraint[_0]]
    assert(toInt[WordList.FilterConstraintList[words, constraints]#length] == 2)
  }

  "constraint list Grey[d]" should "exclude one word" in {
    type constraints = TypeListEnd[Constraint]#Add[Grey[d]#ToConstraint[_0]]#Add[Grey[y]#ToConstraint[_0]]
    assert(toInt[WordList.FilterConstraintList[words, constraints]#length] == 1)
  }

  "constraint list Yellow[d, 0]" should "exclude one word" in {
    // cat does not include d
    type constraints = TypeListEnd[Constraint]#Add[Yellow[d]#ToConstraint[_0]]#Add[Grey[y]#ToConstraint[_0]]
    assert(toInt[WordList.FilterConstraintList[words, constraints]#length] == 1)
  }

  "constraint list Yellow[d, 2]" should "exclude two words" in {
    // cat does not include d, and dog has d in the excluded position
    type constraints = TypeListEnd[Constraint]#Add[Yellow[d]#ToConstraint[_2]]#Add[Grey[y]#ToConstraint[_0]]
    assert(toInt[WordList.FilterConstraintList[words, constraints]#length] == 0)
  }

  "constraint list Green[d, 2]" should "exclude one word" in {
    // cat does not include d
    type constraints = TypeListEnd[Constraint]#Add[Green[d]#ToConstraint[_2]]#Add[Grey[y]#ToConstraint[_0]]
    assert(toInt[WordList.FilterConstraintList[words, constraints]#length] == 1)
  }

  "constraint list Green[d, 0]" should "exclude two words" in {
    // cat does not include d, and dog has d in the wrong position
    type constraints = TypeListEnd[Constraint]#Add[Green[d]#ToConstraint[_0]]#Add[Grey[y]#ToConstraint[_0]]
    assert(toInt[WordList.FilterConstraintList[words, constraints]#length] == 0)
  }
}
