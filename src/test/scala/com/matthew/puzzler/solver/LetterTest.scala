package com.matthew.puzzler.solver

import com.matthew.puzzler.solver.Letter.toLetter
import com.matthew.puzzler.values.Bool._
import org.scalatest.flatspec.AnyFlatSpec

class LetterTest extends AnyFlatSpec {
  "The letter A" should "be equal to itself" in {
    assert(toBoolean[a#matches[a]])
  }

  "The letter A" should "not be equal to another letter" in {
    assert(! toBoolean[a#matches[b]])
  }

  "The letter A" should "be represented by the letter 'a'" in {
    assert(toLetter[a] == "a")
  }
}
