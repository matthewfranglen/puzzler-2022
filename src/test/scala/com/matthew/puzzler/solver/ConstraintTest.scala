package com.matthew.puzzler.solver

import com.matthew.puzzler.solver.Word._
import com.matthew.puzzler.values.Bool.toBoolean
import com.matthew.puzzler.values.Num._0
import org.scalatest.flatspec.AnyFlatSpec

class ConstraintTest extends AnyFlatSpec {
  "Grey[a]" should "match the word dog" in {
    assert(toBoolean[Grey[a]#ToConstraint[_0]#MatchesWord[d :: o ::: g]])
  }

  "Grey[a]" should "not match the word cat" in {
    assert(!toBoolean[Grey[a]#ToConstraint[_0]#MatchesWord[c :: a ::: t]])
  }

  // index zero is LAST letter
  "Yellow[a, 0]" should "match the word cat" in {
    assert(toBoolean[Yellow[a]#ToConstraint[_0]#MatchesWord[c :: a ::: t]])
  }

  "Yellow[a, 0]" should "not match the word abba" in {
    assert(!toBoolean[Yellow[a]#ToConstraint[_0]#MatchesWord[a :: b :: b ::: a]])
  }

  "Green[a, 0]" should "match the word abba" in {
    assert(toBoolean[Green[a]#ToConstraint[_0]#MatchesWord[a :: b :: b ::: a]])
  }

  "Green[a, 0]" should "not match the word cat" in {
    assert(!toBoolean[Green[a]#ToConstraint[_0]#MatchesWord[c :: a ::: t]])
  }
}
