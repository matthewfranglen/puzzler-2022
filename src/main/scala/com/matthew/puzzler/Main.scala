package com.matthew.puzzler

import com.matthew.puzzler.solver.Color.toColor
import com.matthew.puzzler.solver._
import com.matthew.puzzler.values.Num._
import com.matthew.puzzler.solver.Letter.toLetter
import com.matthew.puzzler.values.{TypeList, TypeListEnd}

object Main {
  def main(args: Array[String]): Unit = {
    // This is a fixed problem with a fixed, very short, word list
    // as such I can print out the guesses and conclusion.
    // I can't find a way to print this "properly" using recursion
    // over the guesses so this will have to do.

    // type word = Word.make[a, b, o, r, t]
    type word = Word.make[a, b, a, c, k]
    type result = Solver.Solve[ValidWords.Words, word]
    val guesses = toInt[result#guesses#length]
    type missing = ConstraintList.colors[Word.make[x, x, x, x, x], word]

    print("solution: ")
    print(toLetter[word#head])
    print(toLetter[word#tail#head])
    print(toLetter[word#tail#tail#head])
    print(toLetter[word#tail#tail#tail#head])
    println(toLetter[word#tail#tail#tail#tail#head])

    println("total guesses: " + guesses)

    {
      type guess = result#guesses#SelectIndex[_0]#head
      print("guess 1: ")
      print(toColor[guess#head])
      print(toColor[guess#tail#head])
      print(toColor[guess#tail#tail#head])
      print(toColor[guess#tail#tail#tail#head])
      println(toColor[guess#tail#tail#tail#tail#head])
    }

    if (guesses > 1) {
      type index = result#guesses#SelectIndex[_1]
      type guess = index#IsEmpty#If[
        missing,
        index#head,
        TypeList[Color]
      ]
      print("guess 2: ")
      print(toColor[guess#head])
      print(toColor[guess#tail#head])
      print(toColor[guess#tail#tail#head])
      print(toColor[guess#tail#tail#tail#head])
      println(toColor[guess#tail#tail#tail#tail#head])
    }

    if (guesses > 2) {
      type index = result#guesses#SelectIndex[_2]
      type guess = index#IsEmpty#If[
        missing,
        index#head,
        TypeList[Color]
      ]
      print("guess 3: ")
      print(toColor[guess#head])
      print(toColor[guess#tail#head])
      print(toColor[guess#tail#tail#head])
      print(toColor[guess#tail#tail#tail#head])
      println(toColor[guess#tail#tail#tail#tail#head])
    }

    if (guesses > 3) {
      type index = result#guesses#SelectIndex[_3]
      type guess = index#IsEmpty#If[
        missing,
        index#head,
        TypeList[Color]
      ]
      print("guess 4: ")
      print(toColor[guess#head])
      print(toColor[guess#tail#head])
      print(toColor[guess#tail#tail#head])
      print(toColor[guess#tail#tail#tail#head])
      println(toColor[guess#tail#tail#tail#tail#head])
    }

    if (guesses > 4) {
      type index = result#guesses#SelectIndex[_4]
      type guess = index#IsEmpty#If[
        missing,
        index#head,
        TypeList[Color]
      ]
      print("guess 5: ")
      print(toColor[guess#head])
      print(toColor[guess#tail#head])
      print(toColor[guess#tail#tail#head])
      print(toColor[guess#tail#tail#tail#head])
      println(toColor[guess#tail#tail#tail#tail#head])
    }

    if (guesses > 5) {
      type index = result#guesses#SelectIndex[_5]
      type guess = index#IsEmpty#If[
        missing,
        index#head,
        TypeList[Color]
      ]
      print("guess 6: ")
      print(toColor[guess#head])
      print(toColor[guess#tail#head])
      print(toColor[guess#tail#tail#head])
      print(toColor[guess#tail#tail#tail#head])
      println(toColor[guess#tail#tail#tail#tail#head])
    }
  }
}