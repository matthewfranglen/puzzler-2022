package com.matthew.puzzler.operations

import com.matthew.puzzler.values.{Num, TypeList}

import scala.language.higherKinds

trait UnaryFunction[Input, Output] {
  type Apply[I <: Input] <: Output
}

object UnaryFunction {
  trait OverHead[F <: UnaryFunction[Input, Output], Input, Output] extends UnaryFunction[TypeList[Input], Output] {
    type Apply[L <: TypeList[Input]] = F#Apply[L#head]
  }

  object OverHead {
    type wrap[F <: UnaryFunction[Input, Output], Input, Output] = OverHead[F, Input, Output]
  }

  trait ToConstant[Input, Output] extends UnaryFunction[Input, Output] {
    type Apply[_] = Output
  }

  object ToConstant {
    type to[I, V] = OverHead.wrap[ToConstant[I, V], I, V]
  }

  trait Inc extends UnaryFunction[Num, Num] {
    type Apply[Value <: Num] = Value#inc
  }
}

trait BinaryFunction[Input1, Input2, Output] {
  type Apply[_Input1 <: Input1, _Input2 <: Input2] <: Output
}

object BinaryFunction {
  trait OverHead[F <: BinaryFunction[Left, Right, Output], Left, Right, Output] extends BinaryFunction[TypeList[Left], Right, Output] {
    type Apply[L <: TypeList[Left], R <: Right] = F#Apply[L#head, R]
  }

  object OverHead {
    type wrap[F <: BinaryFunction[Left, Right, Output], Left, Right, Output] = OverHead[F, Left, Right, Output]
  }

  trait Inc[T] extends BinaryFunction[T, Num, Num] {
    type Apply[_, Accumulated <: Num] = Accumulated#inc
  }
}
