package com.matthew.puzzler.operations

case class Equals[A >: B <: B, B]()