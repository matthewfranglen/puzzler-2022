package com.matthew.puzzler.solver

import com.matthew.puzzler.values.{Bool, False, Pair, True, TypeList, TypeListEnd, TypeListEntry}

import scala.language.higherKinds

trait Solver {
  type Apply[
    Words <: TypeList[TypeList[Letter]],
    Solution <: TypeList[Letter]
  ] <: PuzzleResult
}

trait PuzzleResult {
  type Iterate <: PuzzleResult
  type IsSolved <: Bool
  type CanContinue <: Bool

  type guesses <: TypeList[TypeList[Color]]
  type solution <: TypeList[Letter]
}

trait PuzzleUnsolved[
  Guesses <: TypeList[TypeList[Color]],
  Words <: TypeList[TypeList[Letter]],
  Solution <: TypeList[Letter]
] extends PuzzleResult {
  override type Iterate =
    MakeGuess[Guesses]#Apply[Words, Solution]
  override type IsSolved = False
  override type CanContinue = True

  override type guesses = Guesses
  override type solution = Solution
}

trait PuzzleSolved[
  Guesses <: TypeList[TypeList[Color]],
  Solution <: TypeList[Letter]
] extends PuzzleResult {
  override type Iterate = this.type
  override type IsSolved = True
  override type CanContinue = False

  override type guesses = Guesses
  override type solution = Solution
}

trait PuzzleExhausted[
  Guesses <: TypeList[TypeList[Color]],
  Solution <: TypeList[Letter]
] extends PuzzleResult {
  override type Iterate = this.type
  override type IsSolved = False
  override type CanContinue = False

  override type guesses = Guesses
  override type solution = Solution
}

object Solver {
  type Solve[Words <: TypeList[TypeList[Letter]], Solution <: TypeList[Letter]] =
    _Recurse1[
      MakeGuess[TypeListEnd[TypeList[Color]]]#Apply[Words, Solution]
    ]

  type _Recurse1[Result <: PuzzleResult] =
    Result#CanContinue#If[
      _Recurse2[Result#Iterate],
      Result,
      PuzzleResult
    ]

  type _Recurse2[Result <: PuzzleResult] =
    Result#CanContinue#If[
      _Recurse3[Result#Iterate],
      Result,
      PuzzleResult
    ]

  type _Recurse3[Result <: PuzzleResult] =
    Result#CanContinue#If[
      _Recurse4[Result#Iterate],
      Result,
      PuzzleResult
    ]

  type _Recurse4[Result <: PuzzleResult] =
    Result#CanContinue#If[
      _Recurse5[Result#Iterate],
      Result,
      PuzzleResult
    ]

  type _Recurse5[Result <: PuzzleResult] =
    Result#CanContinue#If[
      Result#Iterate,
      Result,
      PuzzleResult
    ]
}

/**
 * This will make the guess by selecting the first word from the list of valid words.
 * If the list of valid words is empty then the list of guesses made to date will be returned.
 *
 * @tparam Guesses the guesses that have been made so far
 */
trait MakeGuess[Guesses <: TypeList[TypeList[Color]]] extends Solver {
  override type Apply[Words <: TypeList[TypeList[Letter]], Solution <: TypeList[Letter]] =
    Words#IsEmpty#If[
      PuzzleExhausted[Guesses, Solution],
      MatchGuess[
        Words#head,
        ConstraintList.colors[Words#head, Solution],
        Guesses
      ]#Apply[Words, Solution],
      PuzzleResult
    ]
}

/**
 * This will take a guess and work out if it is correct.
 * If it is correct then it is added to the guess list and returned.
 *
 * @tparam Submission        the current word to consider
 * @tparam ColoredSubmission the current word with letter coloring
 * @tparam Guesses           the guesses that have been made so far, not including the current submission
 */
trait MatchGuess[
  Submission <: TypeList[Letter],
  ColoredSubmission <: TypeList[Color],
  Guesses <: TypeList[TypeList[Color]]
] extends Solver {
  override type Apply[Words <: TypeList[TypeList[Letter]], Solution <: TypeList[Letter]] =
    Word.MatchesWord[Submission, Solution]#If[
      PuzzleSolved[
        TypeListEntry[ColoredSubmission, Guesses, TypeList[Color]],
        Solution
      ],
      FilterSubmission[
        Submission,
        TypeListEntry[ColoredSubmission, Guesses, TypeList[Color]],
        ConstraintList.constraints[ColoredSubmission, Solution]
      ]#Apply[Words, Solution],
      PuzzleResult
    ]
}

/**
 * This will filter the word list to remove the last submission.
 *
 * @tparam Submission  the current word to consider
 * @tparam Guesses     the guesses that have been made so far, including the current submission
 * @tparam Constraints the word constraints generated from the current submission
 */
trait FilterSubmission[
  Submission <: TypeList[Letter],
  Guesses <: TypeList[TypeList[Color]],
  Constraints <: TypeList[Constraint]
] extends Solver {
  override type Apply[Words <: TypeList[TypeList[Letter]], Solution <: TypeList[Letter]] =
    FilterConstraints[Guesses, Constraints]#Apply[
      Words#tail, // submission is always first word
      // WordList.FilterWord[Words, Submission],
      Solution
    ]
}

/**
 * This will filter the word list to remove any words that do not match the constraints.
 *
 * @tparam Guesses     the guesses that have been made so far, including the current submission
 * @tparam Constraints the word constraints generated from the current submission
 */
trait FilterConstraints[
  Guesses <: TypeList[TypeList[Color]],
  Constraints <: TypeList[Constraint]
] extends Solver {
  override type Apply[Words <: TypeList[TypeList[Letter]], Solution <: TypeList[Letter]] =
    PuzzleUnsolved[
      Guesses,
      WordList.FilterConstraintList[Words, Constraints],
      Solution
    ]
}
