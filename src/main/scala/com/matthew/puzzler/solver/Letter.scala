package com.matthew.puzzler.solver

import com.matthew.puzzler.values.{Bool, False, True}

import scala.language.higherKinds

sealed trait Letter {
  type self <: Letter

  type matches[Other <: Letter] <: Bool

  type isA <: Bool
  type isB <: Bool
  type isC <: Bool
  type isD <: Bool
  type isE <: Bool
  type isF <: Bool
  type isG <: Bool
  type isH <: Bool
  type isI <: Bool
  type isJ <: Bool
  type isK <: Bool
  type isL <: Bool
  type isM <: Bool
  type isN <: Bool
  type isO <: Bool
  type isP <: Bool
  type isQ <: Bool
  type isR <: Bool
  type isS <: Bool
  type isT <: Bool
  type isU <: Bool
  type isV <: Bool
  type isW <: Bool
  type isX <: Bool
  type isY <: Bool
  type isZ <: Bool
}

sealed trait a extends Letter {
  type self = a

  type matches[Other <: Letter] = Other#isA

  type isA = True
  type isB = False
  type isC = False
  type isD = False
  type isE = False
  type isF = False
  type isG = False
  type isH = False
  type isI = False
  type isJ = False
  type isK = False
  type isL = False
  type isM = False
  type isN = False
  type isO = False
  type isP = False
  type isQ = False
  type isR = False
  type isS = False
  type isT = False
  type isU = False
  type isV = False
  type isW = False
  type isX = False
  type isY = False
  type isZ = False
}

sealed trait b extends Letter {
  type self = b

  type matches[Other <: Letter] = Other#isB

  type isA = False
  type isB = True
  type isC = False
  type isD = False
  type isE = False
  type isF = False
  type isG = False
  type isH = False
  type isI = False
  type isJ = False
  type isK = False
  type isL = False
  type isM = False
  type isN = False
  type isO = False
  type isP = False
  type isQ = False
  type isR = False
  type isS = False
  type isT = False
  type isU = False
  type isV = False
  type isW = False
  type isX = False
  type isY = False
  type isZ = False
}

sealed trait c extends Letter {
  type self = c

  type matches[Other <: Letter] = Other#isC

  type isA = False
  type isB = False
  type isC = True
  type isD = False
  type isE = False
  type isF = False
  type isG = False
  type isH = False
  type isI = False
  type isJ = False
  type isK = False
  type isL = False
  type isM = False
  type isN = False
  type isO = False
  type isP = False
  type isQ = False
  type isR = False
  type isS = False
  type isT = False
  type isU = False
  type isV = False
  type isW = False
  type isX = False
  type isY = False
  type isZ = False
}

sealed trait d extends Letter {
  type self = d

  type matches[Other <: Letter] = Other#isD

  type isA = False
  type isB = False
  type isC = False
  type isD = True
  type isE = False
  type isF = False
  type isG = False
  type isH = False
  type isI = False
  type isJ = False
  type isK = False
  type isL = False
  type isM = False
  type isN = False
  type isO = False
  type isP = False
  type isQ = False
  type isR = False
  type isS = False
  type isT = False
  type isU = False
  type isV = False
  type isW = False
  type isX = False
  type isY = False
  type isZ = False
}

sealed trait e extends Letter {
  type self = e

  type matches[Other <: Letter] = Other#isE

  type isA = False
  type isB = False
  type isC = False
  type isD = False
  type isE = True
  type isF = False
  type isG = False
  type isH = False
  type isI = False
  type isJ = False
  type isK = False
  type isL = False
  type isM = False
  type isN = False
  type isO = False
  type isP = False
  type isQ = False
  type isR = False
  type isS = False
  type isT = False
  type isU = False
  type isV = False
  type isW = False
  type isX = False
  type isY = False
  type isZ = False
}

sealed trait f extends Letter {
  type self = f

  type matches[Other <: Letter] = Other#isF

  type isA = False
  type isB = False
  type isC = False
  type isD = False
  type isE = False
  type isF = True
  type isG = False
  type isH = False
  type isI = False
  type isJ = False
  type isK = False
  type isL = False
  type isM = False
  type isN = False
  type isO = False
  type isP = False
  type isQ = False
  type isR = False
  type isS = False
  type isT = False
  type isU = False
  type isV = False
  type isW = False
  type isX = False
  type isY = False
  type isZ = False
}

sealed trait g extends Letter {
  type self = g

  type matches[Other <: Letter] = Other#isG

  type isA = False
  type isB = False
  type isC = False
  type isD = False
  type isE = False
  type isF = False
  type isG = True
  type isH = False
  type isI = False
  type isJ = False
  type isK = False
  type isL = False
  type isM = False
  type isN = False
  type isO = False
  type isP = False
  type isQ = False
  type isR = False
  type isS = False
  type isT = False
  type isU = False
  type isV = False
  type isW = False
  type isX = False
  type isY = False
  type isZ = False
}

sealed trait h extends Letter {
  type self = h

  type matches[Other <: Letter] = Other#isH

  type isA = False
  type isB = False
  type isC = False
  type isD = False
  type isE = False
  type isF = False
  type isG = False
  type isH = True
  type isI = False
  type isJ = False
  type isK = False
  type isL = False
  type isM = False
  type isN = False
  type isO = False
  type isP = False
  type isQ = False
  type isR = False
  type isS = False
  type isT = False
  type isU = False
  type isV = False
  type isW = False
  type isX = False
  type isY = False
  type isZ = False
}

sealed trait i extends Letter {
  type self = i

  type matches[Other <: Letter] = Other#isI

  type isA = False
  type isB = False
  type isC = False
  type isD = False
  type isE = False
  type isF = False
  type isG = False
  type isH = False
  type isI = True
  type isJ = False
  type isK = False
  type isL = False
  type isM = False
  type isN = False
  type isO = False
  type isP = False
  type isQ = False
  type isR = False
  type isS = False
  type isT = False
  type isU = False
  type isV = False
  type isW = False
  type isX = False
  type isY = False
  type isZ = False
}

sealed trait j extends Letter {
  type self = j

  type matches[Other <: Letter] = Other#isJ

  type isA = False
  type isB = False
  type isC = False
  type isD = False
  type isE = False
  type isF = False
  type isG = False
  type isH = False
  type isI = False
  type isJ = True
  type isK = False
  type isL = False
  type isM = False
  type isN = False
  type isO = False
  type isP = False
  type isQ = False
  type isR = False
  type isS = False
  type isT = False
  type isU = False
  type isV = False
  type isW = False
  type isX = False
  type isY = False
  type isZ = False
}

sealed trait k extends Letter {
  type self = k

  type matches[Other <: Letter] = Other#isK

  type isA = False
  type isB = False
  type isC = False
  type isD = False
  type isE = False
  type isF = False
  type isG = False
  type isH = False
  type isI = False
  type isJ = False
  type isK = True
  type isL = False
  type isM = False
  type isN = False
  type isO = False
  type isP = False
  type isQ = False
  type isR = False
  type isS = False
  type isT = False
  type isU = False
  type isV = False
  type isW = False
  type isX = False
  type isY = False
  type isZ = False
}

sealed trait l extends Letter {
  type self = l

  type matches[Other <: Letter] = Other#isL

  type isA = False
  type isB = False
  type isC = False
  type isD = False
  type isE = False
  type isF = False
  type isG = False
  type isH = False
  type isI = False
  type isJ = False
  type isK = False
  type isL = True
  type isM = False
  type isN = False
  type isO = False
  type isP = False
  type isQ = False
  type isR = False
  type isS = False
  type isT = False
  type isU = False
  type isV = False
  type isW = False
  type isX = False
  type isY = False
  type isZ = False
}

sealed trait m extends Letter {
  type self = m

  type matches[Other <: Letter] = Other#isM

  type isA = False
  type isB = False
  type isC = False
  type isD = False
  type isE = False
  type isF = False
  type isG = False
  type isH = False
  type isI = False
  type isJ = False
  type isK = False
  type isL = False
  type isM = True
  type isN = False
  type isO = False
  type isP = False
  type isQ = False
  type isR = False
  type isS = False
  type isT = False
  type isU = False
  type isV = False
  type isW = False
  type isX = False
  type isY = False
  type isZ = False
}

sealed trait n extends Letter {
  type self = n

  type matches[Other <: Letter] = Other#isN

  type isA = False
  type isB = False
  type isC = False
  type isD = False
  type isE = False
  type isF = False
  type isG = False
  type isH = False
  type isI = False
  type isJ = False
  type isK = False
  type isL = False
  type isM = False
  type isN = True
  type isO = False
  type isP = False
  type isQ = False
  type isR = False
  type isS = False
  type isT = False
  type isU = False
  type isV = False
  type isW = False
  type isX = False
  type isY = False
  type isZ = False
}

sealed trait o extends Letter {
  type self = o

  type matches[Other <: Letter] = Other#isO

  type isA = False
  type isB = False
  type isC = False
  type isD = False
  type isE = False
  type isF = False
  type isG = False
  type isH = False
  type isI = False
  type isJ = False
  type isK = False
  type isL = False
  type isM = False
  type isN = False
  type isO = True
  type isP = False
  type isQ = False
  type isR = False
  type isS = False
  type isT = False
  type isU = False
  type isV = False
  type isW = False
  type isX = False
  type isY = False
  type isZ = False
}

sealed trait p extends Letter {
  type self = p

  type matches[Other <: Letter] = Other#isP

  type isA = False
  type isB = False
  type isC = False
  type isD = False
  type isE = False
  type isF = False
  type isG = False
  type isH = False
  type isI = False
  type isJ = False
  type isK = False
  type isL = False
  type isM = False
  type isN = False
  type isO = False
  type isP = True
  type isQ = False
  type isR = False
  type isS = False
  type isT = False
  type isU = False
  type isV = False
  type isW = False
  type isX = False
  type isY = False
  type isZ = False
}

sealed trait q extends Letter {
  type self = q

  type matches[Other <: Letter] = Other#isQ

  type isA = False
  type isB = False
  type isC = False
  type isD = False
  type isE = False
  type isF = False
  type isG = False
  type isH = False
  type isI = False
  type isJ = False
  type isK = False
  type isL = False
  type isM = False
  type isN = False
  type isO = False
  type isP = False
  type isQ = True
  type isR = False
  type isS = False
  type isT = False
  type isU = False
  type isV = False
  type isW = False
  type isX = False
  type isY = False
  type isZ = False
}

sealed trait r extends Letter {
  type self = r

  type matches[Other <: Letter] = Other#isR

  type isA = False
  type isB = False
  type isC = False
  type isD = False
  type isE = False
  type isF = False
  type isG = False
  type isH = False
  type isI = False
  type isJ = False
  type isK = False
  type isL = False
  type isM = False
  type isN = False
  type isO = False
  type isP = False
  type isQ = False
  type isR = True
  type isS = False
  type isT = False
  type isU = False
  type isV = False
  type isW = False
  type isX = False
  type isY = False
  type isZ = False
}

sealed trait s extends Letter {
  type self = s

  type matches[Other <: Letter] = Other#isS

  type isA = False
  type isB = False
  type isC = False
  type isD = False
  type isE = False
  type isF = False
  type isG = False
  type isH = False
  type isI = False
  type isJ = False
  type isK = False
  type isL = False
  type isM = False
  type isN = False
  type isO = False
  type isP = False
  type isQ = False
  type isR = False
  type isS = True
  type isT = False
  type isU = False
  type isV = False
  type isW = False
  type isX = False
  type isY = False
  type isZ = False
}

sealed trait t extends Letter {
  type self = t

  type matches[Other <: Letter] = Other#isT

  type isA = False
  type isB = False
  type isC = False
  type isD = False
  type isE = False
  type isF = False
  type isG = False
  type isH = False
  type isI = False
  type isJ = False
  type isK = False
  type isL = False
  type isM = False
  type isN = False
  type isO = False
  type isP = False
  type isQ = False
  type isR = False
  type isS = False
  type isT = True
  type isU = False
  type isV = False
  type isW = False
  type isX = False
  type isY = False
  type isZ = False
}

sealed trait u extends Letter {
  type self = u

  type matches[Other <: Letter] = Other#isU

  type isA = False
  type isB = False
  type isC = False
  type isD = False
  type isE = False
  type isF = False
  type isG = False
  type isH = False
  type isI = False
  type isJ = False
  type isK = False
  type isL = False
  type isM = False
  type isN = False
  type isO = False
  type isP = False
  type isQ = False
  type isR = False
  type isS = False
  type isT = False
  type isU = True
  type isV = False
  type isW = False
  type isX = False
  type isY = False
  type isZ = False
}

sealed trait v extends Letter {
  type self = v

  type matches[Other <: Letter] = Other#isV

  type isA = False
  type isB = False
  type isC = False
  type isD = False
  type isE = False
  type isF = False
  type isG = False
  type isH = False
  type isI = False
  type isJ = False
  type isK = False
  type isL = False
  type isM = False
  type isN = False
  type isO = False
  type isP = False
  type isQ = False
  type isR = False
  type isS = False
  type isT = False
  type isU = False
  type isV = True
  type isW = False
  type isX = False
  type isY = False
  type isZ = False
}

sealed trait w extends Letter {
  type self = w

  type matches[Other <: Letter] = Other#isW

  type isA = False
  type isB = False
  type isC = False
  type isD = False
  type isE = False
  type isF = False
  type isG = False
  type isH = False
  type isI = False
  type isJ = False
  type isK = False
  type isL = False
  type isM = False
  type isN = False
  type isO = False
  type isP = False
  type isQ = False
  type isR = False
  type isS = False
  type isT = False
  type isU = False
  type isV = False
  type isW = True
  type isX = False
  type isY = False
  type isZ = False
}

sealed trait x extends Letter {
  type self = x

  type matches[Other <: Letter] = Other#isX

  type isA = False
  type isB = False
  type isC = False
  type isD = False
  type isE = False
  type isF = False
  type isG = False
  type isH = False
  type isI = False
  type isJ = False
  type isK = False
  type isL = False
  type isM = False
  type isN = False
  type isO = False
  type isP = False
  type isQ = False
  type isR = False
  type isS = False
  type isT = False
  type isU = False
  type isV = False
  type isW = False
  type isX = True
  type isY = False
  type isZ = False
}

sealed trait y extends Letter {
  type self = y

  type matches[Other <: Letter] = Other#isY

  type isA = False
  type isB = False
  type isC = False
  type isD = False
  type isE = False
  type isF = False
  type isG = False
  type isH = False
  type isI = False
  type isJ = False
  type isK = False
  type isL = False
  type isM = False
  type isN = False
  type isO = False
  type isP = False
  type isQ = False
  type isR = False
  type isS = False
  type isT = False
  type isU = False
  type isV = False
  type isW = False
  type isX = False
  type isY = True
  type isZ = False
}

sealed trait z extends Letter {
  type self = z

  type matches[Other <: Letter] = Other#isZ

  type isA = False
  type isB = False
  type isC = False
  type isD = False
  type isE = False
  type isF = False
  type isG = False
  type isH = False
  type isI = False
  type isJ = False
  type isK = False
  type isL = False
  type isM = False
  type isN = False
  type isO = False
  type isP = False
  type isQ = False
  type isR = False
  type isS = False
  type isT = False
  type isU = False
  type isV = False
  type isW = False
  type isX = False
  type isY = False
  type isZ = True
}

object Letter {
  final class LetterRepresentation[L <: Letter](val value: String)

  implicit val aRepresentation: LetterRepresentation[a] = new LetterRepresentation("a")
  implicit val bRepresentation: LetterRepresentation[b] = new LetterRepresentation("b")
  implicit val cRepresentation: LetterRepresentation[c] = new LetterRepresentation("c")
  implicit val dRepresentation: LetterRepresentation[d] = new LetterRepresentation("d")
  implicit val eRepresentation: LetterRepresentation[e] = new LetterRepresentation("e")
  implicit val fRepresentation: LetterRepresentation[f] = new LetterRepresentation("f")
  implicit val gRepresentation: LetterRepresentation[g] = new LetterRepresentation("g")
  implicit val hRepresentation: LetterRepresentation[h] = new LetterRepresentation("h")
  implicit val iRepresentation: LetterRepresentation[i] = new LetterRepresentation("i")
  implicit val jRepresentation: LetterRepresentation[j] = new LetterRepresentation("j")
  implicit val kRepresentation: LetterRepresentation[k] = new LetterRepresentation("k")
  implicit val lRepresentation: LetterRepresentation[l] = new LetterRepresentation("l")
  implicit val mRepresentation: LetterRepresentation[m] = new LetterRepresentation("m")
  implicit val nRepresentation: LetterRepresentation[n] = new LetterRepresentation("n")
  implicit val oRepresentation: LetterRepresentation[o] = new LetterRepresentation("o")
  implicit val pRepresentation: LetterRepresentation[p] = new LetterRepresentation("p")
  implicit val qRepresentation: LetterRepresentation[q] = new LetterRepresentation("q")
  implicit val rRepresentation: LetterRepresentation[r] = new LetterRepresentation("r")
  implicit val sRepresentation: LetterRepresentation[s] = new LetterRepresentation("s")
  implicit val tRepresentation: LetterRepresentation[t] = new LetterRepresentation("t")
  implicit val uRepresentation: LetterRepresentation[u] = new LetterRepresentation("u")
  implicit val vRepresentation: LetterRepresentation[v] = new LetterRepresentation("v")
  implicit val wRepresentation: LetterRepresentation[w] = new LetterRepresentation("w")
  implicit val xRepresentation: LetterRepresentation[x] = new LetterRepresentation("x")
  implicit val yRepresentation: LetterRepresentation[y] = new LetterRepresentation("y")
  implicit val zRepresentation: LetterRepresentation[z] = new LetterRepresentation("z")

  def toLetter[L <: Letter](implicit letter: LetterRepresentation[L]): String = letter.value
}