package com.matthew.puzzler.solver

import com.matthew.puzzler.values._

import scala.language.higherKinds

sealed trait Color {
  type ToConstraint[Index <: Num] <: Constraint
  type IsGrey <: Bool
  type IsYellow <: Bool
  type IsGreen <: Bool
  type letter <: Letter
}

sealed trait Grey[_Letter <: Letter] extends Color {
  type ToConstraint[Index <: Num] = GreyConstraint[_Letter]

  override type IsGrey = True
  override type IsYellow = False
  override type IsGreen = False
  override type letter = _Letter
}

sealed trait Yellow[_Letter <: Letter] extends Color {
  type ToConstraint[Index <: Num] = YellowConstraint[_Letter, Index]

  override type IsGrey = False
  override type IsYellow = True
  override type IsGreen = False
  override type letter = _Letter
}

sealed trait Green[_Letter <: Letter] extends Color {
  type ToConstraint[Index <: Num] = GreenConstraint[_Letter, Index]

  override type IsGrey = False
  override type IsYellow = False
  override type IsGreen = True
  override type letter = _Letter
}

object Color {
  final class ColorRepresentation[C <: Color](val value: String)

  implicit val greyARepresentation: ColorRepresentation[Grey[a]] = new ColorRepresentation("   Grey[a]")
  implicit val greyBRepresentation: ColorRepresentation[Grey[b]] = new ColorRepresentation("   Grey[b]")
  implicit val greyCRepresentation: ColorRepresentation[Grey[c]] = new ColorRepresentation("   Grey[c]")
  implicit val greyDRepresentation: ColorRepresentation[Grey[d]] = new ColorRepresentation("   Grey[d]")
  implicit val greyERepresentation: ColorRepresentation[Grey[e]] = new ColorRepresentation("   Grey[e]")
  implicit val greyFRepresentation: ColorRepresentation[Grey[f]] = new ColorRepresentation("   Grey[f]")
  implicit val greyGRepresentation: ColorRepresentation[Grey[g]] = new ColorRepresentation("   Grey[g]")
  implicit val greyHRepresentation: ColorRepresentation[Grey[h]] = new ColorRepresentation("   Grey[h]")
  implicit val greyIRepresentation: ColorRepresentation[Grey[i]] = new ColorRepresentation("   Grey[i]")
  implicit val greyJRepresentation: ColorRepresentation[Grey[j]] = new ColorRepresentation("   Grey[j]")
  implicit val greyKRepresentation: ColorRepresentation[Grey[k]] = new ColorRepresentation("   Grey[k]")
  implicit val greyLRepresentation: ColorRepresentation[Grey[l]] = new ColorRepresentation("   Grey[l]")
  implicit val greyMRepresentation: ColorRepresentation[Grey[m]] = new ColorRepresentation("   Grey[m]")
  implicit val greyNRepresentation: ColorRepresentation[Grey[n]] = new ColorRepresentation("   Grey[n]")
  implicit val greyORepresentation: ColorRepresentation[Grey[o]] = new ColorRepresentation("   Grey[o]")
  implicit val greyPRepresentation: ColorRepresentation[Grey[p]] = new ColorRepresentation("   Grey[p]")
  implicit val greyQRepresentation: ColorRepresentation[Grey[q]] = new ColorRepresentation("   Grey[q]")
  implicit val greyRRepresentation: ColorRepresentation[Grey[r]] = new ColorRepresentation("   Grey[r]")
  implicit val greySRepresentation: ColorRepresentation[Grey[s]] = new ColorRepresentation("   Grey[s]")
  implicit val greyTRepresentation: ColorRepresentation[Grey[t]] = new ColorRepresentation("   Grey[t]")
  implicit val greyURepresentation: ColorRepresentation[Grey[u]] = new ColorRepresentation("   Grey[u]")
  implicit val greyVRepresentation: ColorRepresentation[Grey[v]] = new ColorRepresentation("   Grey[v]")
  implicit val greyWRepresentation: ColorRepresentation[Grey[w]] = new ColorRepresentation("   Grey[w]")
  implicit val greyXRepresentation: ColorRepresentation[Grey[x]] = new ColorRepresentation("   Grey[x]")
  implicit val greyYRepresentation: ColorRepresentation[Grey[y]] = new ColorRepresentation("   Grey[y]")
  implicit val greyZRepresentation: ColorRepresentation[Grey[z]] = new ColorRepresentation("   Grey[z]")
  implicit val yellowARepresentation: ColorRepresentation[Yellow[a]] = new ColorRepresentation(" Yellow[a]")
  implicit val yellowBRepresentation: ColorRepresentation[Yellow[b]] = new ColorRepresentation(" Yellow[b]")
  implicit val yellowCRepresentation: ColorRepresentation[Yellow[c]] = new ColorRepresentation(" Yellow[c]")
  implicit val yellowDRepresentation: ColorRepresentation[Yellow[d]] = new ColorRepresentation(" Yellow[d]")
  implicit val yellowERepresentation: ColorRepresentation[Yellow[e]] = new ColorRepresentation(" Yellow[e]")
  implicit val yellowFRepresentation: ColorRepresentation[Yellow[f]] = new ColorRepresentation(" Yellow[f]")
  implicit val yellowGRepresentation: ColorRepresentation[Yellow[g]] = new ColorRepresentation(" Yellow[g]")
  implicit val yellowHRepresentation: ColorRepresentation[Yellow[h]] = new ColorRepresentation(" Yellow[h]")
  implicit val yellowIRepresentation: ColorRepresentation[Yellow[i]] = new ColorRepresentation(" Yellow[i]")
  implicit val yellowJRepresentation: ColorRepresentation[Yellow[j]] = new ColorRepresentation(" Yellow[j]")
  implicit val yellowKRepresentation: ColorRepresentation[Yellow[k]] = new ColorRepresentation(" Yellow[k]")
  implicit val yellowLRepresentation: ColorRepresentation[Yellow[l]] = new ColorRepresentation(" Yellow[l]")
  implicit val yellowMRepresentation: ColorRepresentation[Yellow[m]] = new ColorRepresentation(" Yellow[m]")
  implicit val yellowNRepresentation: ColorRepresentation[Yellow[n]] = new ColorRepresentation(" Yellow[n]")
  implicit val yellowORepresentation: ColorRepresentation[Yellow[o]] = new ColorRepresentation(" Yellow[o]")
  implicit val yellowPRepresentation: ColorRepresentation[Yellow[p]] = new ColorRepresentation(" Yellow[p]")
  implicit val yellowQRepresentation: ColorRepresentation[Yellow[q]] = new ColorRepresentation(" Yellow[q]")
  implicit val yellowRRepresentation: ColorRepresentation[Yellow[r]] = new ColorRepresentation(" Yellow[r]")
  implicit val yellowSRepresentation: ColorRepresentation[Yellow[s]] = new ColorRepresentation(" Yellow[s]")
  implicit val yellowTRepresentation: ColorRepresentation[Yellow[t]] = new ColorRepresentation(" Yellow[t]")
  implicit val yellowURepresentation: ColorRepresentation[Yellow[u]] = new ColorRepresentation(" Yellow[u]")
  implicit val yellowVRepresentation: ColorRepresentation[Yellow[v]] = new ColorRepresentation(" Yellow[v]")
  implicit val yellowWRepresentation: ColorRepresentation[Yellow[w]] = new ColorRepresentation(" Yellow[w]")
  implicit val yellowXRepresentation: ColorRepresentation[Yellow[x]] = new ColorRepresentation(" Yellow[x]")
  implicit val yellowYRepresentation: ColorRepresentation[Yellow[y]] = new ColorRepresentation(" Yellow[y]")
  implicit val yellowZRepresentation: ColorRepresentation[Yellow[z]] = new ColorRepresentation(" Yellow[z]")
  implicit val greenARepresentation: ColorRepresentation[Green[a]] = new ColorRepresentation("  Green[a]")
  implicit val greenBRepresentation: ColorRepresentation[Green[b]] = new ColorRepresentation("  Green[b]")
  implicit val greenCRepresentation: ColorRepresentation[Green[c]] = new ColorRepresentation("  Green[c]")
  implicit val greenDRepresentation: ColorRepresentation[Green[d]] = new ColorRepresentation("  Green[d]")
  implicit val greenERepresentation: ColorRepresentation[Green[e]] = new ColorRepresentation("  Green[e]")
  implicit val greenFRepresentation: ColorRepresentation[Green[f]] = new ColorRepresentation("  Green[f]")
  implicit val greenGRepresentation: ColorRepresentation[Green[g]] = new ColorRepresentation("  Green[g]")
  implicit val greenHRepresentation: ColorRepresentation[Green[h]] = new ColorRepresentation("  Green[h]")
  implicit val greenIRepresentation: ColorRepresentation[Green[i]] = new ColorRepresentation("  Green[i]")
  implicit val greenJRepresentation: ColorRepresentation[Green[j]] = new ColorRepresentation("  Green[j]")
  implicit val greenKRepresentation: ColorRepresentation[Green[k]] = new ColorRepresentation("  Green[k]")
  implicit val greenLRepresentation: ColorRepresentation[Green[l]] = new ColorRepresentation("  Green[l]")
  implicit val greenMRepresentation: ColorRepresentation[Green[m]] = new ColorRepresentation("  Green[m]")
  implicit val greenNRepresentation: ColorRepresentation[Green[n]] = new ColorRepresentation("  Green[n]")
  implicit val greenORepresentation: ColorRepresentation[Green[o]] = new ColorRepresentation("  Green[o]")
  implicit val greenPRepresentation: ColorRepresentation[Green[p]] = new ColorRepresentation("  Green[p]")
  implicit val greenQRepresentation: ColorRepresentation[Green[q]] = new ColorRepresentation("  Green[q]")
  implicit val greenRRepresentation: ColorRepresentation[Green[r]] = new ColorRepresentation("  Green[r]")
  implicit val greenSRepresentation: ColorRepresentation[Green[s]] = new ColorRepresentation("  Green[s]")
  implicit val greenTRepresentation: ColorRepresentation[Green[t]] = new ColorRepresentation("  Green[t]")
  implicit val greenURepresentation: ColorRepresentation[Green[u]] = new ColorRepresentation("  Green[u]")
  implicit val greenVRepresentation: ColorRepresentation[Green[v]] = new ColorRepresentation("  Green[v]")
  implicit val greenWRepresentation: ColorRepresentation[Green[w]] = new ColorRepresentation("  Green[w]")
  implicit val greenXRepresentation: ColorRepresentation[Green[x]] = new ColorRepresentation("  Green[x]")
  implicit val greenYRepresentation: ColorRepresentation[Green[y]] = new ColorRepresentation("  Green[y]")
  implicit val greenZRepresentation: ColorRepresentation[Green[z]] = new ColorRepresentation("  Green[z]")

  def toColor[C <: Color](implicit color: ColorRepresentation[C]): String = color.value
}
