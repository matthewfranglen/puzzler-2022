package com.matthew.puzzler.solver

import com.matthew.puzzler.operations.UnaryFunction
import com.matthew.puzzler.values.{Bool, TypeList}

object ConstraintList {
  type colors[Submission <: TypeList[Letter], Solution <: TypeList[Letter]] =
    Submission#Map[ColorLetterFunction[Solution], Color]

  type constraints[Submission <: TypeList[Color], Solution <: TypeList[Letter]] =
    Submission#Map[ConstraintFunction, Constraint]

  type FilterWord[_ConstraintList <: TypeList[Constraint], _Word <: TypeList[Letter]] =
    _ConstraintList#MatchAllHead[WordMatchesConstraintFunction[_Word]]

  trait ColorLetterFunction[Solution <: TypeList[Letter]] extends UnaryFunction[TypeList[Letter], Color] {
    override type Apply[Head <: TypeList[Letter]] =
      Word.MatchesLetter[Solution, Head#head, Head#length#dec]#If[
        Green[Head#head],
        Word.ContainsLetter[Solution, Head#head]#If[
          Yellow[Head#head],
          Grey[Head#head],
          Color
        ],
        Color
      ]
  }

  trait ConstraintFunction extends UnaryFunction[TypeList[Color], Constraint] {
    override type Apply[Head <: TypeList[Color]] =
      Head#head#ToConstraint[Head#length#dec]
  }

  trait WordMatchesConstraintFunction[_Word <: TypeList[Letter]] extends UnaryFunction[Constraint, Bool] {
    override type Apply[_Constraint <: Constraint] =
      _Constraint#MatchesWord[_Word]
  }
}
