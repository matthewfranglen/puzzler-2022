package com.matthew.puzzler.solver

import com.matthew.puzzler.operations.UnaryFunction
import com.matthew.puzzler.values.Bool.Not
import com.matthew.puzzler.values.{Bool, TypeList, TypeListEnd, TypeListEntry}

object WordList {
  type ::[_Word <: TypeList[Letter], Tail <: TypeList[TypeList[Letter]]] =
    TypeListEntry[_Word, Tail, TypeList[Letter]]
  type :::[Word1 <: TypeList[Letter], Word2 <: TypeList[Letter]] =
    TypeListEntry[Word1, TypeListEntry[Word2, TypeListEnd[TypeList[Letter]], TypeList[Letter]], TypeList[Letter]]

  type FilterConstraint[_WordList <: TypeList[TypeList[Letter]], _Constraint <: Constraint] =
    _WordList#FilterHead[ConstraintFunction[_Constraint]]

  type FilterConstraintList[_WordList <: TypeList[TypeList[Letter]], _ConstraintList <: TypeList[Constraint]] =
    _WordList#FilterHead[ConstraintListFunction[_ConstraintList]]

  type FilterWord[_WordList <: TypeList[TypeList[Letter]], _Word <: TypeList[Letter]] =
    _WordList#FilterHead[WordFunction[_Word]]

  trait ConstraintFunction[_Constraint <: Constraint] extends UnaryFunction[TypeList[Letter], Bool] {
    override type Apply[_Word <: TypeList[Letter]] = _Constraint#MatchesWord[_Word]
  }

  trait ConstraintListFunction[_ConstraintList <: TypeList[Constraint]] extends UnaryFunction[TypeList[Letter], Bool] {
    override type Apply[_Word <: TypeList[Letter]] =
      ConstraintList.FilterWord[_ConstraintList, _Word]
  }

  trait WordFunction[_Word <: TypeList[Letter]] extends UnaryFunction[TypeList[Letter], Bool] {
    override type Apply[W <: TypeList[Letter]] =
      Not[Word.MatchesWord[_Word, W]]
  }
}