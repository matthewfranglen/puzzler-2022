package com.matthew.puzzler.solver

import com.matthew.puzzler.values.Bool.Not
import com.matthew.puzzler.values.{Bool, False, Num, TypeList}

import scala.language.higherKinds

/**
 * The constraint expresses knowledge about the letters and their positions.
 * The MatchesWord method determines if the constraint passes for the given word.
 */
trait Constraint {
  type MatchesWord[W <: TypeList[Letter]] <: Bool

  type letter <: Letter
}

/**
 * This is a letter which does not appear in the solution.
 *
 * @tparam _Letter the letter that is not in the solution
 */
trait GreyConstraint[_Letter <: Letter] extends Constraint {
  override type MatchesWord[W <: TypeList[Letter]] =
    Not[Word.ContainsLetter[W, _Letter]]

  override type letter = _Letter
}

/**
 * This is a letter which is in the solution but _not_ at a specific position.
 *
 * @tparam _Letter the letter that is in the solution
 * @tparam Index   the position that the letter must not be in
 */
trait YellowConstraint[_Letter <: Letter, Index <: Num] extends Constraint {
  override type MatchesWord[W <: TypeList[Letter]] =
    Word.ContainsLetter[W, _Letter]#If[
      Not[Word.MatchesLetter[W, _Letter, Index]],
      False,
      Bool
    ]

  override type letter = _Letter
}

/**
 * This is a letter which appears at a known position in the solution.
 *
 * @tparam _Letter the letter that is in the solution
 * @tparam Index   the position of the letter in the solution
 */
trait GreenConstraint[_Letter <: Letter, Index <: Num] extends Constraint {
  override type MatchesWord[W <: TypeList[Letter]] =
    Word.MatchesLetter[W, _Letter, Index]

  override type letter = _Letter
}