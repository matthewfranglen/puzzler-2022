package com.matthew.puzzler.solver

import com.matthew.puzzler.operations.UnaryFunction
import com.matthew.puzzler.solver.Letter._
import com.matthew.puzzler.values._

import scala.language.higherKinds

object Word {
  type make[Letter1 <: Letter, Letter2 <: Letter, Letter3 <: Letter, Letter4 <: Letter, Letter5 <: Letter] =
    Letter1 :: Letter2 :: Letter3 :: Letter4 ::: Letter5
  type ::[_Letter <: Letter, Tail <: TypeList[Letter]] =
    TypeListEntry[_Letter, Tail, Letter]
  type :::[Letter1 <: Letter, Letter2 <: Letter] =
    TypeListEntry[Letter1, TypeListEntry[Letter2, TypeListEnd[Letter], Letter], Letter]

  type MatchesWord[Left <: TypeList[Letter], Right <: TypeList[Letter]] =
    Left#length#Compare[Right#length]#eq#If[
      Left#Zip[Right, Letter]#MatchAllHead[PairMatchFunction],
      False,
      Bool
    ]

  type ContainsLetter[_Word <: TypeList[Letter], _Letter <: Letter] =
    _Word#MatchAnyHead[LetterMatchFunction[_Letter]]

  type MatchesLetter[_Word <: TypeList[Letter], _Letter <: Letter, Index <: Num] =
    _Word#SelectIndex[Index]#ApplyHead[LetterMatchFunction[_Letter], False, Bool]

  trait PairMatchFunction extends UnaryFunction[Pair[Letter, Letter], Bool] {
    type Apply[Value <: Pair[Letter, Letter]] =
      Value#left#matches[Value#right]
  }

  trait LetterMatchFunction[_Letter <: Letter] extends UnaryFunction[Letter, Bool] {
    type Apply[Value <: Letter] =
      _Letter#matches[Value]
  }

  // implicit def wordRepresentationEnd: WordRepresentation[TypeListEnd[Letter]] = new WordRepresentation[TypeListEnd[Letter]]("")

  // implicit def wordRepresentationN[W <: TypeListEntry[Letter, _, Letter]](
  //   implicit rep: WordRepresentation[W]
  // ): WordRepresentation[W] =
  //   new WordRepresentation[W](toLetter[W#head])

  // final class WordRepresentation[W <: TypeList[Letter]](val value: String)

  // def toString[W <: TypeList[Letter]](implicit rep: WordRepresentation[W]): String = rep.value
}