package com.matthew.puzzler.values

trait Constant[V] {
  type Value[_] = V
}
