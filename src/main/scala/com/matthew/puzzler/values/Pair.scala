package com.matthew.puzzler.values

trait Pair[Left <: Any, Right <: Any] {
  type left <: Left
  type right <: Right
}

trait PairImpl[Left <: LeftUB, Right <: RightUB, LeftUB, RightUB] extends Pair[LeftUB, RightUB] {
  override type left = Left
  override type right = Right
}