package com.matthew.puzzler.values

import com.matthew.puzzler.operations.{BinaryFunction, UnaryFunction}
import com.matthew.puzzler.values.Num._

import scala.language.higherKinds

trait TypeList[T] {
  /**
   * Transform the value at this point using the provided function.
   *
   * @tparam F           The function to convert the value
   * @tparam IfEmpty     The value to return if this list is empty
   * @tparam _UpperBound The upper bound of the value that will be returned
   */
  type Apply[F <: UnaryFunction[TypeList[T], _UpperBound], IfEmpty <: _UpperBound, _UpperBound] <: _UpperBound
  /**
   * Transform the value at this point using the provided function.
   *
   * @tparam F           The function to convert the value
   * @tparam IfEmpty     The value to return if this list is empty
   * @tparam _UpperBound The upper bound of the value that will be returned
   */
  type ApplyHead[F <: UnaryFunction[T, _UpperBound], IfEmpty <: _UpperBound, _UpperBound] =
    Apply[UnaryFunction.OverHead.wrap[F, T, _UpperBound], IfEmpty, _UpperBound]
  /**
   * Perform a left fold over the values to return an overall value.
   * This will operate over head before tail.
   *
   * @tparam F           The function to combine the accumulated value with the current value
   * @tparam Initial     The value to pass to the function for the first invocation
   * @tparam _UpperBound The upper bound of the value that will be returned, the initial value, and the accumulated value
   */
  type FoldL[F <: BinaryFunction[TypeList[T], _UpperBound, _UpperBound], Initial <: _UpperBound, _UpperBound] <: _UpperBound
  /**
   * Perform a left fold over the values to return an overall value.
   * This will operate over head before tail.
   *
   * @tparam F           The function to combine the accumulated value with the current value
   * @tparam Initial     The value to pass to the function for the first invocation
   * @tparam _UpperBound The upper bound of the value that will be returned, the initial value, and the accumulated value
   */
  type FoldLHead[F <: BinaryFunction[T, _UpperBound, _UpperBound], Initial <: _UpperBound, _UpperBound] =
    FoldL[BinaryFunction.OverHead.wrap[F, T, _UpperBound, _UpperBound], Initial, _UpperBound]
  /**
   * Perform a right fold over the values to return an overall value.
   * This will operate over tail before head.
   *
   * @tparam F           The function to combine the accumulated value with the current value
   * @tparam Initial     The value to pass to the function for the first invocation
   * @tparam _UpperBound The upper bound of the value that will be returned, the initial value, and the accumulated value
   */
  type FoldR[F <: BinaryFunction[TypeList[T], _UpperBound, _UpperBound], Initial <: _UpperBound, _UpperBound] <: _UpperBound
  /**
   * Perform a right fold over the values to return an overall value.
   * This will operate over tail before head.
   *
   * @tparam F           The function to combine the accumulated value with the current value
   * @tparam Initial     The value to pass to the function for the first invocation
   * @tparam _UpperBound The upper bound of the value that will be returned, the initial value, and the accumulated value
   */
  type FoldRHead[F <: BinaryFunction[T, _UpperBound, _UpperBound], Initial <: _UpperBound, _UpperBound] =
    FoldR[BinaryFunction.OverHead.wrap[F, T, _UpperBound, _UpperBound], Initial, _UpperBound]
  /**
   * Combine the two lists creating a list of pairs.
   * Will consume both lists from the left until one is empty.
   * This list becomes the left values, the other list becomes the right values.
   *
   * @tparam Other The list to zip with
   */
  type Zip[Other <: TypeList[R], R] <: TypeList[Pair[T, R]]

  /**
   * Transform the list using the provided function.
   *
   * @tparam F The function to convert the values
   */
  type Map[F <: UnaryFunction[TypeList[T], _UpperBound], _UpperBound] =
    FoldR[TypeList.MapFunction[F, T, _UpperBound], TypeListEnd[_UpperBound], TypeList[_UpperBound]]
  /**
   * Transform the list using the provided function.
   *
   * @tparam F The function to convert the values
   */
  type MapHead[F <: UnaryFunction[T, _UpperBound], _UpperBound] =
    Map[UnaryFunction.OverHead.wrap[F, T, _UpperBound], _UpperBound]
  /**
   * Reduce the list to those elements which pass the predicate.
   *
   * @tparam F The function that will test each element of the type list
   */
  type Filter[F <: UnaryFunction[TypeList[T], Bool]] =
    FoldR[TypeList.FilterFunction[F, T], TypeListEnd[T], TypeList[T]]
  /**
   * Reduce the list to those elements which pass the predicate.
   *
   * @tparam F The function that will test each element of the type list
   */
  type FilterHead[F <: UnaryFunction[T, Bool]] =
    Filter[UnaryFunction.OverHead.wrap[F, T, Bool]]
  /**
   * Test if every element of the list passes the predicate.
   *
   * @tparam F The function that will test each element of the type list
   */
  type MatchAll[F <: UnaryFunction[TypeList[T], Bool]] =
    FoldR[TypeList.MatchAllFunction[F, T], True, Bool]
  /**
   * Test if every element of the list passes the predicate.
   *
   * @tparam F The function that will test each element of the type list
   */
  type MatchAllHead[F <: UnaryFunction[T, Bool]] =
    MatchAll[UnaryFunction.OverHead.wrap[F, T, Bool]]
  /**
   * Test if any element of the list passes the predicate.
   *
   * @tparam F The function that will test each element of the type list
   */
  type MatchAny[F <: UnaryFunction[TypeList[T], Bool]] =
    FoldR[TypeList.MatchAnyFunction[F, T], False, Bool]
  /**
   * Test if any element of the list passes the predicate.
   *
   * @tparam F The function that will test each element of the type list
   */
  type MatchAnyHead[F <: UnaryFunction[T, Bool]] =
    MatchAny[UnaryFunction.OverHead.wrap[F, T, Bool]]
  /**
   * Get the type list at this index
   *
   * @tparam N The index to select
   */
  type SelectIndex[N <: Num] =
    FoldL[TypeList.SelectFunction[N, T], TypeListEnd[T], TypeList[T]]

  type Add[Other <: T] <: TypeList[T]
  type Concatenate[Other <: TypeList[T]] <: TypeList[T]

  type head <: T
  type tail <: TypeList[T]
  type length <: Num
  type IsEmpty <: Bool
}

trait TypeListEnd[T] extends TypeList[T] {
  override type Apply[F <: UnaryFunction[TypeList[T], _UpperBound], IfEmpty <: _UpperBound, _UpperBound] = IfEmpty
  override type FoldL[F <: BinaryFunction[TypeList[T], _UpperBound, _UpperBound], Initial <: _UpperBound, _UpperBound] = Initial
  override type FoldR[F <: BinaryFunction[TypeList[T], _UpperBound, _UpperBound], Initial <: _UpperBound, _UpperBound] = Initial
  override type Zip[Other <: TypeList[R], R] = TypeListEnd[Pair[T, R]]
  override type Add[Other <: T] = TypeListEntry[Other, this.type, T]
  override type Concatenate[Other <: TypeList[T]] = Other

  override type head = Nothing
  override type tail = Nothing
  override type length = _0
  override type IsEmpty = True
}

trait TypeListEntry[Head <: UB, Tail <: TypeList[UB], UB] extends TypeList[UB] {
  override type Apply[F <: UnaryFunction[TypeList[UB], _UpperBound], IfEmpty <: _UpperBound, _UpperBound] =
    F#Apply[this.type]
  override type FoldL[F <: BinaryFunction[TypeList[UB], _UpperBound, _UpperBound], Initial <: _UpperBound, _UpperBound] =
    Tail#FoldL[
      F,
      F#Apply[this.type, Initial],
      _UpperBound,
    ]
  override type FoldR[F <: BinaryFunction[TypeList[UB], _UpperBound, _UpperBound], Initial <: _UpperBound, _UpperBound] =
    F#Apply[
      this.type,
      Tail#FoldR[F, Initial, _UpperBound]
    ]
  type Zip[Other <: TypeList[R], R] =
    Other#IsEmpty#If[
      TypeListEnd[Pair[UB, R]],
      TypeListEntry[
        PairImpl[Head, Other#head, UB, R],
        Tail#Zip[Other#tail, R],
        Pair[UB, R]
      ],
      TypeList[Pair[UB, R]]
    ]
  override type Add[Other <: UB] =
    TypeListEntry[Other, this.type, UB]
  override type Concatenate[Other <: TypeList[UB]] =
    TypeListEntry[Head, Tail#Concatenate[Other], UB]

  override type head = Head
  override type tail = Tail
  override type length = FoldLHead[BinaryFunction.Inc[UB], _0, Num]
  override type IsEmpty = False
}

object TypeList {
  type Range[N <: Num] = N#range

  trait FilterFunction[F <: UnaryFunction[TypeList[T], Bool], T] extends BinaryFunction[TypeList[T], TypeList[T], TypeList[T]] {
    type Apply[Value <: TypeList[T], Accumulated <: TypeList[T]] =
      F#Apply[Value]#If[
        TypeListEntry[Value#head, Accumulated, T],
        Accumulated,
        TypeList[T]
      ]
  }

  trait MapFunction[F <: UnaryFunction[TypeList[T], _ <: R], T, R] extends BinaryFunction[TypeList[T], TypeList[R], TypeList[R]] {
    type Apply[Value <: TypeList[T], Accumulated <: TypeList[R]] =
      TypeListEntry[F#Apply[Value], Accumulated, R]
  }

  trait MatchAnyFunction[F <: UnaryFunction[TypeList[T], Bool], T] extends BinaryFunction[TypeList[T], Bool, Bool] {
    type Apply[Value <: TypeList[T], Accumulated <: Bool] =
      Accumulated#If[True, F#Apply[Value], Bool]
  }

  trait MatchAllFunction[F <: UnaryFunction[TypeList[T], Bool], T] extends BinaryFunction[TypeList[T], Bool, Bool] {
    type Apply[Value <: TypeList[T], Accumulated <: Bool] =
      Accumulated#If[F#Apply[Value], False, Bool]
  }

  trait SelectFunction[N <: Num, T] extends BinaryFunction[TypeList[T], TypeList[T], TypeList[T]] {
    type Apply[Value <: TypeList[T], Accumulated <: TypeList[T]] =
      Value#IsEmpty#If[
        Accumulated,
        N#Compare[Value#length#dec]#eq#If[
          Value,
          Accumulated,
          TypeList[T]
        ],
        TypeList[T]
      ]
  }
}