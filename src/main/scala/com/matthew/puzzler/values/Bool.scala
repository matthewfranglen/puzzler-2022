package com.matthew.puzzler.values

import scala.language.higherKinds

// https://apocalisp.wordpress.com/2010/06/13/type-level-programming-in-scala-part-3-boolean/

sealed trait Bool {
  type If[T <: UpperBound, F <: UpperBound, UpperBound] <: UpperBound
}

sealed trait True extends Bool {
  type If[T <: UpperBound, F <: UpperBound, UpperBound] = T
}

sealed trait False extends Bool {
  type If[T <: UpperBound, F <: UpperBound, UpperBound] = F
}

object Bool {
  type &&[A <: Bool, B <: Bool] = A#If[B, False, Bool]
  type ||[A <: Bool, B <: Bool] = A#If[True, B, Bool]
  type Not[A <: Bool] = A#If[False, True, Bool]

  final class BoolRepresentation[B <: Bool](val value: Boolean)

  implicit val falseRepresentation: BoolRepresentation[False] = new BoolRepresentation(false)
  implicit val trueRepresentation: BoolRepresentation[True] = new BoolRepresentation(true)

  def toBoolean[B <: Bool](implicit b: BoolRepresentation[B]): Boolean = b.value
}