package com.matthew.puzzler.values

import com.matthew.puzzler.operations.{Comparison, EQ, GT, LT}

import scala.language.higherKinds

sealed trait Num {
  type Match[NonZero[I <: Num] <: UpperBound, IfZero <: UpperBound, UpperBound] <: UpperBound
  type Compare[I <: Num] <: Comparison
  type Add[I <: Num] <: Num

  type inc <: Num
  type dec <: Num
  type range <: TypeList[Num]
}

sealed trait ZeroNum extends Num {
  type Match[NonZero[I <: Num] <: UpperBound, IfZero <: UpperBound, UpperBound] = IfZero
  type Compare[I <: Num] = I#Match[Constant[LT]#Value, EQ, Comparison]
  type Add[I <: Num] = I

  type inc = NonZeroNum[ZeroNum]
  type dec = ZeroNum
  type range = TypeListEnd[Num]
}

sealed trait NonZeroNum[OneLess <: Num] extends Num {
  type Match[NonZero[I <: Num] <: UpperBound, IfZero <: UpperBound, UpperBound] = NonZero[OneLess]
  type Compare[I <: Num] = I#Match[OneLess#Compare, GT, Comparison]
  type Add[I <: Num] = NonZeroNum[OneLess#Add[I]]

  type inc = NonZeroNum[NonZeroNum[OneLess]]
  type dec = OneLess
  type range = TypeListEntry[NonZeroNum[OneLess], TypeList.Range[OneLess], Num]
}

object Num {
  type _0 = ZeroNum
  type _1 = NonZeroNum[_0]
  type _2 = NonZeroNum[_1]
  type _3 = NonZeroNum[_2]
  type _4 = NonZeroNum[_3]
  type _5 = NonZeroNum[_4]

  type +[A <: Num, B <: Num] = A#Add[B]

  // stopping condition
  implicit def indexRepresentation0: NumRepresentation[_0] = new NumRepresentation[_0](0)
  implicit def indexRepresentationN[N <: Num](implicit rep: NumRepresentation[N]): NumRepresentation[NonZeroNum[N]] = rep.oneMore

  final class NumRepresentation[N <: Num](val value: Int) {
    def oneMore: NumRepresentation[NonZeroNum[N]] = new NumRepresentation[NonZeroNum[N]](value + 1)
  }

  // this converts the type representation to a number
  def toInt[I <: Num](implicit rep: NumRepresentation[I]): Int = rep.value
}