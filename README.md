Puzzler 2022
============

The puzzler this year is to implement an automatic wordle solver.
There is a fixed set of some 11,000 words to use for this challenge, and the ideal challenge involves guessing each one.

The extra credit is to have an average score of less than 3.5.

Approach
--------

I like to solve these puzzlers using unusual techniques.
This time I am going to implement this using the Scala generic type system.

It may surprise you to know that programming using types has some problems.
Even counting to 10 requires 11 types (peano numbers).
Scala has to resolve all of these types and there is a limited stack for these operations.
This heavily limits how clever my solution can be.

The original challenge also had a second extra credit option to create the "worst" solver that would try to fail.
This solver has to use all known facts about the solution at any point (i.e. must use all green letters in the correct location, must include all yellow letters etc).
It happens that this is a well defined solver that does not heavily depend on math and information theory, so that is what I am implementing.

The optimally bad approach is to select the word which gives the least information at each point.
There are videos around using information theory to choose the most informative word, which may not adhere to the currently known constraints (e.g. use a word that has 5 unknown letters to test them all).
Given this choosing the words that always adhere to the known constraints is nearly ideally bad.
The second stage would be to select the word that provides the least additional information, which is likely to be the ones containing the letters that are shared with the least other words.
Given that even counting to 10 is difficult computing letter counts over a large set of words is totally infeasible, not to speak of information theoretic calculations.
Instead I have chosen to take the very first word in the word list after filtering it by the known constraints.

Even this is incredibly heavy.
With 24 possible words the compilation time of the complete solver is over 5 minutes.
30 words takes more than an hour (I stopped it before it completed).
The original wordle had ~2,300 solutions so this might need a bit of work before it can handle that.

Example Output:

```
solution: aback
total guesses: 3
guess 1:   Green[a]   Grey[d]   Grey[o] Yellow[b]   Grey[e]
guess 2:   Green[a]  Green[b]   Grey[y]   Grey[s]   Grey[s]
guess 3:   Green[a]  Green[b]  Green[a]  Green[c]  Green[k]
```

Gory Details
------------

This might help if you are unfamiliar with programming type systems and their capabilities.

### An Introduction to Type Systems

A type system is an expression of the values that a computer program operates over.
It can be used to find programming errors, where one part of the system produces a value which cannot be handled by
another.
Since the program can handle many concrete values the generic system describes the values by their kind (e.g. integers
and strings) instead of their value (e.g. 1 and "hello").

We can describe this in Scala by using the class name of the type, e.g. String or Integer.

The basic type system runs into problems when you have functions and collections.

### Collection Values

A collection is something like a `List` or a `Set`.
It contains values.
Describing the type of the collection is easy, but how do we describe the type of the contained values?

When you put a value into a list and then get it back, do you get something of the same type?
This depends on the generic type of the list.
If we have a type hierarchy of `Any` -> `Animal` -> `Cat` then we can define a `List[Animal]` (a list of animals).

What does this type restrict?
It restricts what we can put in, and in exchange we can know what we get out of the list.

Is a list of animals a list of anything?
No, if we add a string then when we get values from the list they cannot just be animals.

Is a list of animals a list of cats?
No, if we expect only cats from a list that also contains dogs then we will run into problems.

So a list has a generic type which cannot be varied once defined.
We can describe this in Scala by using the square brackets to define the generic type of the list, e.g. `List[Cat]`.
As mentioned once defined this list is _invariant_ in the generic type as it cannot be changed.

### Function Arguments and Return Values

A function object may be described by the type system by defining the type of the arguments that it accepts and
produces.
To handle different functions we need to be able to accept functions that vary in their arguments and results.
As long as the function could accept any value that matches the type definition then it should be an acceptable value
for the type.

If we have a type hierarchy of `Any` -> `Animal` -> `Cat` then we can define a function signature which
is `(Cat) => Animal`.
This describes a function which can take any cat and will return an animal.

If we pass a function which is `(Cat) => Cat` then this is an acceptable function, as we know that all cats are animals.
This means that the return type of the function is _covariant_, as the type can be substituted for any subtype.

We can describe this in Scala by adding the + to the type name, for example `(Cat) => +Animal`.
This means that any subtype of the type is acceptable.

If we pass a function which is `(Animal) => Animal` then this is an acceptable function, as we still know that all cats
are animals.
Our type description of this function is more restrictive than the actual function, however the actual function conforms
to the type.
This means that the argument type of the function is _contravariant_, as the type can be substituted for any supertype.

We can describe this in Scala by adding the - to the type name, for example `(-Cat) => Animal`.
This means that any supertype of the type is acceptable.

### Generic Type Bounds

How do we define a list to allow it to work with any type?
We can define the class with a named type such as `class List[T]`.
Then we can define methods within the class which use this named generic type like `def get(int index): T`.

Sometimes we want to define one generic type based on another generic type.
If we have a method which returns a type which has a relationship to another generic type then we have to describe the
relationship between two generic types.
This means that our unary type operators of `+` and `-` are no longer sufficient.

We can express the invariance, covariance and contravariance between two types, `T` and `U` as follows:

* invariance is expressed using `=:=`, so `U =:= T`. This means that the type `U` is the same as the type `T` and no
  superclass or subclass is permitted. This is rarely useful as you could just use the original type.
* covariance is expressed using `<:`, so `U <: T`. This means that the type `U` is the same as or a subclass of `T`.
* contravariance is expressed using `>:`, so `U >: T`. This means that the type `U` is the same as or a superclass
  of `T`.

## Computation using the Type System

All of this is not quite enough to be turing complete as you are still restricted to a single type.
What is required is a way to define a type where the resolution of that type can invoke another type.
To do this in Scala you need to be able to define one type in terms of another.

For example, we can imagine a `StringMap[Value]` which is a `Map[Key, Value]` but the `Key` is always `String`.
To define this we can write `type StringMap[Value] = Map[String, Value]`.

If we want to make this into a loop then we can define a positive integer type which is based on a zero value and then
wrappers, where each wrapper adds one.
To define this we write:

```scala
trait Number

trait Zero extends Number

trait NotZero[N <: Number] extends Number
```

We can then define some numbers as follows:

```scala
type One = NotZero[Zero]
type Two = NotZero[One]
type Three = NotZero[Two]
```

To make this useful we can then define a "function" which resolves as one of two values based on the current type being
considered.

```scala
trait Number {
  type IsZero[True <: UpperBound, False <: UpperBound, UpperBound] <: UpperBound
}

trait Zero extends Number {
  type IsZero[True <: UpperBound, False <: UpperBound, UpperBound] = True
}

trait NotZero[N <: Number] extends Number {
  type IsZero[True <: UpperBound, False <: UpperBound, UpperBound] = False
}
```

Invoking this is done through another type, using `#` in a way similar to using `.` over concrete objects:

```scala
// this compiles which means that the result type is a Long
Three#IsZero[Int, Long, Number] =:= Long
```

A much better description of programming using Scala types can be found on the [apocalypse blog](https://apocalisp.wordpress.com/2010/06/08/type-level-programming-in-scala/).
This README really introduces the basic syntax and the approach.
The key thing to remember is traversal through one type allows for conditional branches, and nested types allows for looping.
